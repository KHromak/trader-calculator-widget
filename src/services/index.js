import axios from "axios";

class DataCalculate {
    constructor() {
        this.response = null;
        this._apiBase = "";
    };


    getResources = (url) => {
        this.response = axios.get(url).then((response) => {
            if (response.status !== 200) {
                throw new Error(`Could not response ${url}, status ${response.status}`)
            }
            return response.data;
        });
        return this.response
    };

    getTypeAccount = () => {
        return this.getResources('/account.json');
    }
}

export default DataCalculate;