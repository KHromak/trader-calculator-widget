import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import DropDown from '../drop-down/drop-down';
import RadioBtn from '../radio-btn'
import './account-setup.css';
import ElementInput from "../element-input";

class AccountSetup extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        console.log('-- didMount --');
    }

    render() {
        console.log(this.props.itemList);
        return (

            <div className="account-setup">
                <div className="close-component">X</div>
                <div className="wrapper-account-setup">
                    <p className="account-setup__item">№</p>
                    <p className="account-setup__num">1</p>
                </div>
                <div className="left-block">
                    <DropDown label={"Инструменты"} itemList={this.props.tools}size={227}/>
                    <div className="wrapper-type-transaction">
                        <p className="volume">Тип сделки</p>
                        <RadioBtn firstTitle="Sell" lastTitle="Buy"/>
                    </div>
                </div>
                <div className="right-block">
                    <div className="wrapper-open-price">
                        <div className="wrapper-open-price__volume">
                            <p className="volume">Объем:</p>
                            <RadioBtn firstTitle="Лот" lastTitle="Валютный" />
                        </div>
                        <DropDown size={377} />
                    </div>
                    <div className="input-wrapper">
                        <ElementInput titleName="Цена открытия" />
                        <ElementInput titleName="Цена закрытия" />
                    </div>
                </div>
            </div>
        );
    }
}

// Указываем  редюсер который мы берем
const mapStateToProps = ({}) => ({});

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {},
        dispatch
    );

export default connect(mapStateToProps, mapDispatchToProps)(AccountSetup);
