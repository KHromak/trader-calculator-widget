import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import './drop-down.css';
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import {setValue} from '../../actions/account-type';


export default function SimpleSelect(props) {

    const useStyles = makeStyles(theme => ({
        formControl: {
            margin: theme.spacing(1),
            minWidth: props.size,
        },
        selectEmpty: {
            marginTop: theme.spacing(2),
        }
    }));


    const classes = useStyles();
    const [item, setAge, id] = React.useState('');

    const inputLabel = React.useRef(null);
    const [
        labelWidth, setLabelWidth
    ] = React.useState(0);
    React.useEffect(() => {
        setLabelWidth(inputLabel.current.offsetWidth);
    }, []);

    const handleChange = (event, option) => {

        setValue(option.key);
        setAge(event.target.value);
    };

    console.log(props.valueSelect);


    function renderItemList() {

        if (props.itemList !== undefined) {
            return props.itemList.map((type) => {
                return <MenuItem value={type.title} key={type.id}>{type.title}</MenuItem>;
            });
        }
    }


    return (
        <div className="Drop-Down">
            <p className="drop-down-label">{props.label}</p>
            <FormControl variant="outlined" className={classes.formControl}>
                <InputLabel ref={inputLabel} id="demo-simple-select-outlined-label">
                    {props.label}
                </InputLabel>
                <Select
                    labelId="demo-simple-select-outlined-label"
                    id="demo-simple-select-outlined"
                    value={item}
                    onChange={handleChange}
                    labelWidth={labelWidth}
                >
                    {renderItemList()}
                </Select>
            </FormControl>
        </div>
    );
}

// Указываем  редюсер который мы берем (достаем)
const mapStateToProps = ({accountType}) => ({

    valueSelect: accountType.valueSelect
});

// const mapDispatchToProps = (dispatch) => ({
//     getAccountSuccess: accountType => dispatch(getAccountSuccess(accountType))
// });
//(вкладываем)
const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            setValue
        },
        dispatch
    );

 connect(mapStateToProps, mapDispatchToProps)(SimpleSelect);
