import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import './app.css';
import DropDown from '../drop-down/drop-down';
import {getAccountSuccess} from '../../actions/account-type';
import DataCalculate from '../../services'
import Header from '../header/header'
import AccountSetup from "../account-setup/account-setup";

class App extends Component {
    constructor(props) {
        super(props);
        this.dataCalculate = new DataCalculate();
    }

    componentDidMount() {
        this.updateData();
    }

    updateData = () => {
        // action creator
        const {getAccountSuccess} = this.props;
        this.dataCalculate.getTypeAccount().then((response) => {
            getAccountSuccess(response)
        });
    };

    render() {
        const {accountType} = this.props;
        return (
            <div className="app">
                <div className="container">
                    <header className="app-header">
                        <Header/>
                    </header>
                </div>
                <section className="app-drop-down">
                    <div className="container">
                        <p className="setting-title">Настройки счета</p>
                        <div className="wrapper-drop-down">
                            <DropDown itemList={accountType['accountType']} label={"Платформа"} size={280} />
                            <DropDown itemList={accountType['typeCurrency']} label={"Тип счета"} size={280}/>
                            <DropDown itemList={accountType['currency']} label={"Валюта счета"} size={280}/>
                        </div>
                    </div>
                </section>
                <section className="app-account-setup">
                    <div className="container">
                        <p className="account-setup-title">Настройки счета</p>
                        <AccountSetup  tools={accountType['currency']} label={"Платформа"} size={280} />
                    </div>
                </section>
            </div>
        );
    }
}

// Указываем  редюсер который мы берем (достаем)
const mapStateToProps = ({accountType}) => ({
    accountType: accountType.items
});

// const mapDispatchToProps = (dispatch) => ({
//     getAccountSuccess: accountType => dispatch(getAccountSuccess(accountType))
// });
//(вкладываем)
const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            getAccountSuccess
        },
        dispatch
    );

export default connect(mapStateToProps, mapDispatchToProps)(App);
