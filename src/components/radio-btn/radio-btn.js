import React from 'react';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';

export default function FormControlLabelPosition(props) {
    const [value, setValue] = React.useState('female');

    const handleChange = event => {
        setValue(event.target.value);
    };

    return (
        <FormControl component="fieldset">
            <RadioGroup aria-label="position" name="position" value={value} onChange={handleChange} row>
                <FormControlLabel
                    value="end"
                    control={<Radio color="primary" />}
                    label={props.firstTitle}
                    labelPlacement="end"
                />
                <FormControlLabel
                    value="end"
                    control={<Radio color="primary" />}
                    label={props.lastTitle}
                    labelPlacement="end"
                />
            </RadioGroup>
        </FormControl>
    );
}
