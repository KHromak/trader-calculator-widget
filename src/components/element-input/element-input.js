import React, {Component} from "react";
import '../element-input/element-input.css';

class ElementInput extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: "15"
        }
    }

    render() {
        return (
            <div className="input-calculate">
                <p className="title-input">
                    {this.props.titleName}
                </p>
                <input type="text" className="input-calculate__item" value={this.state.value}/>
            </div>
        );
    }
}

export default ElementInput;