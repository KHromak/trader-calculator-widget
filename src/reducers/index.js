import {combineReducers} from 'redux';
import accountType from './accountType';

export default combineReducers({
    accountType
});