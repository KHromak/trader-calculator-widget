import {GET_TYPE_ACCOUNT_SUCCESS} from '../constans';

const initialState = {
    isReady: false,
    items: [],
    valueSelect: 1

};

export default (state = initialState, action) => {
    const {type, payload} = action;
    switch (type) {
        case GET_TYPE_ACCOUNT_SUCCESS:
            return {
                ...state,
                items: action.response,
                isReady: true
            };
        case 'SET_IS_READY':
            return {
                ...state,
                isReady: payload
            };
        case 'SET_SELECT_VALUE':
            return {
                ...state,
                valueSelect: action.value,
            };
        default:
            return state;
    }
};

