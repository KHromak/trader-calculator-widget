import { GET_TYPE_ACCOUNT_SUCCESS,
         GET_PLATFORM_SUCCESS,
         GET_CURRENCY_SUCCESS
} from '../constans'

export const getAccountSuccess = (response) => ({
    type: GET_TYPE_ACCOUNT_SUCCESS,
    response
});

export const getPlatform = (response) => ({
   type : GET_PLATFORM_SUCCESS,
   response
});

export const getCurrency = (response) => ({
    type : GET_CURRENCY_SUCCESS,
    response
});

export const setValue = (value) => ({
    type : 'SET_SELECT_VALUE',
        value
});



